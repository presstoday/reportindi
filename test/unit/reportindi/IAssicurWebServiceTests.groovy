package reportindi



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(IAssicurWebService)
class IAssicurWebServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
