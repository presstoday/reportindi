
(function($) {
    
    $.fn.quickSelect = function() {
        return $(this).click(function() { $(this).select(); });
    };
    
    $.fn.integerField = function() {
        return $(this).keypress(function(event) {
            if(event.keyCode != 13) {
                var c = String.fromCharCode(event.which);
                if(!/^[0-9\-]$/.test(c)) event.preventDefault();
            }
        });
    };
    
    $.fn.decimalField = function(format, allowBlank) {
        return $(this).keypress(function(event) {
            if(event.keyCode != 13) {
                var c = String.fromCharCode(event.which);
                if(!/^[0-9\-.,]$/.test(c)) event.preventDefault();
            }
        }).blur(function() { 
            var value = this.value;
            //if(/\d+.\d+/.test(value)) value = value.replace(/\./, ',');
            if(allowBlank && value == "") return;
            else this.value = numeral(value).format(format || '0,0.00[0000]');
        }).blur().quickSelect();
    };
    
    $.fn.errorsField = function() {
        return $(this).tooltip({
            tooltipClass: 'errors-tip', 
            content: function() { return this.title; },
            position: {my: 'center bottom', at: 'center top-10'},
            hide: false,
            show: false
        });
    };

    $.fn.uppercase = function() {
        return $(this).keyup(function() {
            this.value = this.value.toUpperCase();
        });
    };

    $.fn.shiftSelectable = function() {
        var lastChecked, boxes = this;
        boxes.click(function(evt) {
            if(!lastChecked) {
                lastChecked = this;
                return;
            }
            if(evt.shiftKey) {
                var start = boxes.index(this), end = boxes.index(lastChecked);
                boxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked).trigger('change');
            }
            lastChecked = this;
        });
    };

})(jQuery);


