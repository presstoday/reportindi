package reportindi

import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil as GHU
import org.springframework.beans.SimpleTypeConverter as STC

class FormTagLib {

    static namespace = "f"

    def grailsApplication

    def field = { attrs, body ->
        if(attrs.containsKey('bean') && !attrs.bean) throwTagError('Tag [field] requires a non-null value for attribute [bean]')
        def bean = attrs.remove('bean')
        if(bean instanceof String) bean = pageScope.variables[bean]
        if(bean && attrs.containsKey('name') && !attrs.name) throwTagError('Tag [field] requires a non-null value for attribute [name]')
        def name = attrs.name
        def id = attrs.id = attrs.id ?: name?.tokenize('.[]')?.join('-') ?: randomId("$attrs.type-")
        //attrs.type = attrs.type ?: 'text'
        attrs.value = bean && name ? (beanProperty(bean, name) ?: attrs.value) : attrs.value
        def labelAttrs = attrsForPrefix(attrs, 'label')
        addHtmlClass(labelAttrs, 'field-label')
        def required = attrs.remove('required') ?: false
        if(required) addHtmlClass(labelAttrs, 'required')
        labelAttrs.for = id
        if(attrs.containsKey('required') && attrs.remove('required') != false) labelAttrs.class = ((labelAttrs.class?.split(' ') ?: []) + 'required').join(' ')
        addHtmlClass(attrs, 'field-content ui-widget-content ui-corner-all')
        def hasFieldError = isDomain(bean) && bean.hasErrors() && name && bean.errors.hasFieldErrors(name.endsWith('id') ? name[0..-4] : name)
        def fieldAttrs = attrsForPrefix(attrs, 'field')
        if(hasFieldError) {
            addHtmlClass(fieldAttrs, 'field error')
            def errors = bean.errors.getFieldErrors(name.endsWith('id') ? name[0..-4] : name)
            fieldAttrs.title = ui.list(data: errors.collect { error -> message(error: error) }, class: 'error-list', 'li-class': 'error')
        } else addHtmlClass(fieldAttrs, 'field')
        def html = new HtmlBuilder(out)
        html.div(fieldAttrs) {
            def label = attrs.remove('label') ?: uncamelize(name) ?: ''
            if(label && attrs.type != 'hidden') html.label(labelAttrs) { mkp.yieldUnescaped label }
            if(attrs.type == 'text' || attrs.type == 'file') mkp.yieldUnescaped input(attrs)
            else if(attrs.type == 'date') mkp.yieldUnescaped date(attrs)
            else if(attrs.type == 'password') mkp.yieldUnescaped password(attrs)
            else if(attrs.type == 'decimal') mkp.yieldUnescaped decimal(attrs)
            else if(attrs.type == 'currency') mkp.yieldUnescaped currency(attrs)
            else if(attrs.type == 'integer') mkp.yieldUnescaped integer(attrs)
            else if(attrs.type == 'uppercase') mkp.yieldUnescaped uppercase(attrs)
            else if(attrs.type == 'checkbox') mkp.yieldUnescaped checkbox(attrs)
            else if(attrs.type == 'radio') mkp.yieldUnescaped radio(attrs)
            else if(attrs.type == 'select') mkp.yieldUnescaped select(attrs)
            else if(attrs.type == 'search') mkp.yieldUnescaped search(attrs)
            else if(attrs.type == 'output') mkp.yieldUnescaped output(attrs)
            else if(attrs.type == 'hidden') mkp.yieldUnescaped hidden(attrs)
            else if(attrs.type == 'textarea') mkp.yieldUnescaped textarea(attrs)
            mkp.yieldUnescaped body()?.trim()
        }
    }

    def input = { attrs ->
        attrs.type = attrs.type ?: 'text'
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId("$attrs.type-")
        def html = new HtmlBuilder(out)
        html.input(attrs)
        /*if(!(attrs.type in ['checkbox', 'radio', 'submit', 'file']) && !request.xhr) {
            r.script { /$("#$id").quickSelect();/ }
        }*/
    }

    def button = { attrs, body ->
        attrs.type = attrs.type ?: 'button'
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId("$attrs.type-")
        def text = body()?.trim() ?: attrs.remove("value")
        def buttonOptions = ['disabled', 'icons', 'label', 'text']
        def options = jsOptions(attrs, buttonOptions)
        def html = new HtmlBuilder(out)
        html.button(attrs) { mkp.yieldUnescaped text }
        r.script { /$("#$id").button($options);/ }
    }

    def hidden = { attrs ->
        attrs.type = 'hidden'
        out << input(attrs)
    }

    def checkbox = { attrs ->
        attrs.type = 'checkbox'
        if(attrs.value) attrs.checked = "checked"
        out << input(attrs)
    }

    def radio = { attrs ->
        attrs.type = 'radio'
        out << input(attrs)
    }

    def password = { attrs ->
        attrs.type = 'password'
        out << input(attrs)
    }

    def decimal = { attrs ->
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('decimal-')
        attrs.type = 'text'
        def decimalFormat = attrs.containsKey('decimalFormat') ? /"${attrs.remove('decimalFormat')}"/ : 'null'
        def allowBlank = attrs.remove('allowBlank') != 'true' ? false : true
        out << input(attrs)
        r.script() { /$("#$id").decimalField($decimalFormat, $allowBlank);/ }
    }

    def currency = { attrs ->
        attrs.decimalFormat = '0,0[.]00 $'
        out << decimal(attrs)
    }

    def integer = { attrs ->
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('decimal-')
        attrs.type = 'text'
        out << input(attrs)
        r.script() { /$("#$id").integerField();/ }
    }

    def uppercase = { attrs ->
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('uppercase-')
        attrs.type = 'text'
        addHtmlClass(attrs, 'uppercase')
        out << input(attrs)
        r.script() { /$("#$id").uppercase();/ }
    }

    def select = { attrs ->
        def data = attrs.remove('from')
        if(data == null) throwTagError('Tag [select] missing required attribute [from]')
        if(data instanceof String) data = pageScope.variables[data]
        if(attrs.type) attrs.remove('type')
        def groupBy = attrs.containsKey('groupBy') && attrs.groupBy ? attrs.remove('groupBy') : false
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('select-')
        def enhanced = attrs.remove('enhanced') != 'false' ? true : false
        def search = attrs.remove('search') == 'false' ? -1 : 10
        def value = attrs.remove('value')
        def first = attrs.remove('first')
        if(first == null) first = 'Scegli...'
        else if(first == 'false') first = false
        def optionKey = attrs.remove('optionKey'), optionValue = attrs.remove('optionValue')
        if(value instanceof Collection) attrs.multiple = true
        removeHtmlClass(attrs, "ui-widget-content", "ui-corner-all")
        def renderOption = { item ->
            item = GHU.unwrapIfProxy(item)
            def k, v, selected = null
            if(item) {
                if(optionKey) {
                    if(optionKey instanceof Closure) k = optionKey(item)
                    else k = beanProperty(item, optionKey)
                } else if(isDomain(item)) k = item.id
                else if(item instanceof Map.Entry) k = item.key
                else if(item instanceof Enum) k = item.name()
                else k = item
                if(optionValue) {
                    if(optionValue instanceof Closure) v = optionValue(item)
                    else v = beanProperty(item, optionValue)
                } else if(item instanceof Map.Entry) v = item.value
                else v = item
                if(value instanceof Collection && value.contains(k)) selected = "selected"
                else if(isDomain(item) && isDomain(value) && value.id == k) selected = "selected"
                else if(value instanceof Enum && value.name() == k) selected = "selected"
                else if(value?.getClass()?.isInstance(k) && value == k) selected = "selected"
                else if(value == k) selected = "selected"
                else if(k?.getClass() && value != null) {
                    try {
                        def typeConverter = new STC()
                        value = typeConverter.convertIfNecessary(value, k.getClass())
                        if(k == value)  selected = "selected"
                    } catch (e) { /*e.printStackTrace()*/ }
                }
                option(value: k, selected: selected) { mkp.yieldUnescaped v }
            }
        }
        def html = new HtmlBuilder(out)
        renderOption.delegate = html
        html.select(attrs) {
            if(first) option(value: 'null') { mkp.yieldUnescaped first }
            if(groupBy) {
                if(groupBy instanceof Closure) data = data.groupBy groupBy
                else data = data.groupBy { it[groupBy] }
                data.each { label, items -> optgroup(label: label) { items.each { item -> renderOption(item) } } }
            } else data.each { item -> renderOption(item) }
        }
        //if(enhanced) r.script() { /$("#$id").select2({minimumResultsForSearch: $search});/ }
    }

    def output = { attrs ->
        attrs.type = null
        attrs.name = null
        attrs.disabled = "disabled"
        attrs.rows = 1
        addHtmlClass(attrs, 'output')
        def id = attrs.id = attrs.id ?: randomId('output-')
        def value = attrs.remove('value') ?: ''
        def html = new HtmlBuilder(out)
        def renderAs = attrs.remove("renderAs") ?: "label"
        html."$renderAs"(attrs) { mkp.yieldUnescaped value }
        if(renderAs == "textarea") r.script() { /$("#$id").css("height", $("#$id").prop("scrollHeight"));/ }
    }

    def date = { attrs ->
        def datePickerOptions = [
                'altField', 'altFormat', 'appendText', 'autoSize', 'beforeShow',
                'beforeShowDay', 'buttonImage', 'buttonImageOnly', 'buttonText', 'calculateWeek',
                'changeMonth', 'changeYear', 'closeText', 'constrainInput', 'currentText',
                'dateFormat', 'dayNames', 'dayNamesMin', 'dayNamesShort', 'defaultDate',
                'duration', 'firstDay', 'gotoCurrent', 'hideIfNoPrevNext', 'isRTL',
                'maxDate', 'minDate', 'monthNames', 'monthNamesShort', 'navigationAsDateFormat',
                'nextText', 'numberOfMonths', 'onChangeMonthYear', 'onClose', 'onSelect',
                'prevText', 'selectOtherMonths', 'shortYearCutoff', 'showAnim', 'showButtonPanel',
                'showCurrentAtPos', 'showMonthAfterYear', 'showOn', 'showOptions', 'showOtherMonths',
                'showWeek', 'stepMonths', 'weekHeader', 'yearRange', 'yearSuffix'
        ]
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('date-picker-')
        attrs.type = 'text'
        attrs.dateFormat = attrs.remove('format') ?: 'dd-mm-yy'
        def options = jsOptions(attrs, datePickerOptions)
        def html = new HtmlBuilder(out)
        html.input(attrs)
        r.script() { /$("#$id").datepicker($options);/ }
    }

    def submit = { attrs ->
        def buttonOptions = ['disabled', 'icons', 'label', 'text', 'create']
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('submit-')
        attrs.type = 'submit'
        def options = jsOptions(attrs, buttonOptions)
        out << input(attrs)
        r.script() { /$("#$id").button($options);/ }
    }

    def search = { attrs ->
        attrs.name = attrs.name ?: 'q'
        attrs.type = 'text'
        attrs.class = ((attrs.class?.split(' ') ?: []) + 'search').join(' ')
        attrs.value = params[attrs.name]
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId('search-')
        out << input(attrs) << '<span class="ui-icon ui-icon-search clickable"></span>'
        r.script() { /
            $("#$id").keypress(function(event) { if(event.which == 13) document.location.href = updateParameter("q", this.value != '' ? encodeURIComponent(this.value) : undefined); });
            $("#$id+.ui-icon").click(function() { document.location.href = updateParameter("q", $("#$id").val() != '' ? encodeURIComponent($("#$id").val()) : undefined); });
        / }
    }

    def textarea = { attrs ->
        def id = attrs.id = attrs.id ?: attrs.name?.tokenize('.[]')?.join('-') ?: randomId("$attrs.type-")
        def value = attrs.remove('value') ?: ''
        def html = new HtmlBuilder(out)
        html.textarea(attrs) { mkp.yieldUnescaped value }
        r.script() { /$("#$id").css("height", $("#$id").prop("scrollHeight"));/ }
    }

    private randomId(prefix = '') {
        if(request['random-id'] == null) request['random-id'] = []
        def id = prefix + String.random(10)
        while(id in request['random-id']) id = prefix + String.random(10)
        request['random-id'] << id
        return id
    }

    private jsOptions(attrs, options = []) {
        /*def found = attrs.findAll { k, v -> k in options }
        found.each { k, v -> attrs.remove(k) }
        return '{' + found.collect { k, v ->
            def value = v
            if(v instanceof String && v.startsWith('function')) value = /$v/
            else if(v instanceof String) value = /"$v"/
            /"$k": $value/
        }.join(', ') + '}'*/
        def found = attrs.findAll { k, v -> k in options }, parse
        found.each { k, v -> attrs.remove(k) }
        parse = { map ->
            return '{' + map.collect { k, v ->
                def value = v
                if(v instanceof String && v.startsWith('function')) value = /$v/
                else if(v instanceof String) value = /"$v"/
                else if(v instanceof Map) value = parse(v)
                /$k: $value/
            }.join(', ') + '}'
        }
        return parse(found)
    }

    private beanProperty(bean, property) {
        def path = property.tokenize(".[]")
        def value = bean
        for(sub in path) value = value != null ? value[sub.isNumber() ? sub as int : sub] : null
        if(value instanceof Date || value instanceof BigDecimal) value = value.format()
        return value
    }

    private attrsForPrefix(attrs, prefix) {
        def found = attrs.findAll { k, v -> k.startsWith("$prefix-") }
        found.each { k, v -> attrs.remove(k) }
        found.collectEntries { k, v -> [k.replace("$prefix-", ''), v] }
    }

    private addHtmlClass(attrs, String... htmlClass) { attrs.class = ((attrs.class?.split(' ') ?: []) + htmlClass.join(' ')).join(' ') }

    private removeHtmlClass(attrs, String... htmlClass) { attrs.class = (attrs.class?.split(' ') ?: []).findAll { clazz -> !(clazz in htmlClass) }.join(' ') }

    private isDomain(bean) { return bean && grailsApplication.isDomainClass(bean.getClass()) }

    private uncamelize(str) { str?.split('\\.')?.last()?.replaceAll(/\B[A-Z]/) { ' ' + it.toLowerCase() }?.capitalize() }

}
