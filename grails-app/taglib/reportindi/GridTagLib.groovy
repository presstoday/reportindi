package reportindi

import grails.orm.PagedResultList as PRL

class GridTagLib {

    static namespace = "grid"

    static def TABLE = "grid:table"
    static def CAPTION = "grid:caption"
    static def ROWS = "grid:rows"
    static def COLUMNS = "grid:columns"
    static def FOOTER = "grid:footer"

    def table = { attrs, body ->
        if(attrs.data == null) throwTagError('Tag [table] requires a non-null value for attribute [data]')
        def data = attrs.remove('data')
        if(data instanceof String) data = pageScope.variables[data]
        pageScope.variables[TABLE] = [:]
        pageScope.variables[TABLE][COLUMNS] = []
        body()
        def trAttrs = attrsForPrefix(attrs, 'tr'), thAttrs = attrsForPrefix(attrs, 'th'), tdAttrs = attrsForPrefix(attrs, 'td')
        def caption = pageScope.variables[TABLE][CAPTION] ?: attrs.caption ? "<caption>${attrs.remove('caption')}</caption>" : false
        def columns = pageScope.variables[TABLE][COLUMNS]
        def headers = attrs.remove('headers') != 'false' ? true : false
        def emptyMessage = attrs.remove('emptyMessage') ?: 'Nessun elemento presente'
        def paginate = attrs.remove('paginate') != 'true' ? false : true
        addHtmlClass(attrs, 'grid-table')
        def html = new HtmlBuilder(out)
        html.table(attrs) {
            if(caption) mkp.yieldUnescaped caption
            colgroup() { columns.each { column -> html.col(column.col) } }
            if(headers) thead() { tr(trAttrs) { columns.each { column -> html.th(mergeHtmlAttrs(thAttrs, column.thAttrs, column.attrs)) {
                if(column.sortable) {
                    def link = [
                        controller: controllerName,
                        action: actionName,
                        params: [
                            sort: column.name,
                            order: params.sort == column.name && params.order != 'desc' ? 'desc' : 'asc'
                        ] + params.findAll { k, v -> !(k in ['sort', 'order'])}
                    ], icon = params.sort != column.name ? 'sort-icon ui-icon ui-icon-triangle-2-n-s' :
                              params.order == 'desc' ? 'sort-icon ui-icon ui-icon-triangle-1-n' : 'sort-icon ui-icon ui-icon-triangle-1-s'
                    html.a(href: createLink(link)) {
                        span(class: icon) { mkp.yield '' }
                        mkp.yieldUnescaped column.header
                    }
                }
                else mkp.yieldUnescaped column.header
            } } } }
            tbody() {
                data.each { row ->
                    html.tr(trAttrs) {
                        columns.each { column -> html."$column.tag"(mergeHtmlAttrs(tdAttrs, column.tdAttrs, column.attrs)) { mkp.yieldUnescaped column.content(row) } }
                    }
                }
                if(data.size() == 0) {
                    addHtmlClass(trAttrs, 'empty-table-row')
                    tr(trAttrs) { td(mergeHtmlAttrs(tdAttrs, [colspan: columns.size(), class: 'centered-column'])) { mkp.yieldUnescaped emptyMessage } }
                }
            }
            if(paginate && data instanceof PRL) {
                tfoot() { tr() { td(colspan: columns.size()) {
                    def max = params.remove('max'), offset = params.remove('offset'), total = data.totalCount, pages = ["0": 1], pagina = 1
                    for(def o = max; o < total; o += max) pages["$o"] = ++pagina
                    html.form(method: 'get', id: 'paginate-form') {
                        params.each { k, v -> if(!(k in ['action', 'controller', 'id'])) mkp.yieldUnescaped f.hidden(name: k, value: v) }
                        div(id: 'max-field') {
                            mkp.yieldUnescaped f.field(type: 'integer', name: 'max', label: 'Mostra', value: max)
                            label() { mkp.yield "risultati su $total" }
                        }
                        if(pages.size() > 1) div(id: 'offset-field') {
                            def first = pages["${offset - max}"] ? 0 : false, prev = pages["${offset - max}"] ? offset - max : false, next = pages["${offset + max}"] ? offset + max : false, last = pages["${offset + max}"] ? pages.max { it.key }.key : false
                            mkp.yieldUnescaped ui.link(action: actionName, params: params + [max: max, offset: first], class: "offset-link ui-state-default ${first == false ? 'ui-state-disabled' : ''} ui-corner-all", disabled: first == false, icon: 'ui-icon-seek-first', title: 'Prima pagina')
                            mkp.yieldUnescaped ui.link(action: actionName, params: params + [max: max, offset: prev], class: "offset-link ui-state-default ${prev == false ? 'ui-state-disabled' : ''} ui-corner-all", disabled: prev == false, icon: 'ui-icon-seek-prev', title: 'Pagina precedente')
                            mkp.yieldUnescaped f.field(type: 'select', name: 'offset', label: 'Pagina', value: offset, from: pages, first: false)
                            label() { mkp.yield "di ${pages.size()}" }
                            mkp.yieldUnescaped ui.link(action: actionName, params: params + [max: max, offset: next], class: "offset-link ui-state-default ${next == false ? 'ui-state-disabled' : ''} ui-corner-all", disabled: next == false, icon: 'ui-icon-seek-next', title: 'Pagina successiva')
                            mkp.yieldUnescaped ui.link(action: actionName, params: params + [max: max, offset: last], class: "offset-link ui-state-default ${last == false ? 'ui-state-disabled' : ''} ui-corner-all", disabled: last == false, icon: 'ui-icon-seek-end', title: 'Ultima pagina')

                        }
                    }
                    r.script() { /
                        var paginate = function() { $("#paginate-form").submit(); };
                        $("#max", "#max-field").change(paginate);
                        $("#offset", "#offset-field").change(paginate);
                    / }
                } } }
            }
        }
    }

    def caption = { attrs, body ->
        def content = attrs.remove('text') ?: body()?.trim() ?: ''
        def caption = new StringWriter()
        new HtmlBuilder(caption).caption(attrs) { mkp.yieldUnescaped content }
        pageScope.variables[TABLE][CAPTION] = caption
    }

    def column = { attrs, body ->
        def column = [:], var = attrs.remove('var') ?: 'value'
        column.tag = attrs.remove('tag') != 'th' ? 'td' : 'th'
        column.name = attrs.remove('name') ?: ''
        column.value = attrs.remove('value')
        column.header = attrs.remove('header') ?: uncamelize(column.name) ?: ''
        column.emptyMessage = attrs.remove('emptyMessage') ?: ''
        def col = attrs.col && attrs.col instanceof Map ? attrs.remove('col') : attrsForPrefix(attrs, 'col') ?: [:]
        column.col =  [id: col?.id ?: attrs.name?.tokenize('.[]')?.join('-')] + col
        addHtmlClass(column.col, 'table-column')
        column.thAttrs = attrsForPrefix(attrs, 'th')
        column.tdAttrs = attrsForPrefix(attrs, 'td')
        column.attrs = attrs
        column.content = { row ->
            if(column.name) body((var): beanProperty(row, column.name))?.trim() ?: beanProperty(row, column.name) ?: column.emptyMessage
            else if(column.value) column.value
            else body((var): row)?.trim() ?: row ?: column.emptyMessage
        }
        column.sortable = attrs.remove('sortable') != 'true' ? false : true
        pageScope.variables[TABLE][COLUMNS] << column
    }

    private beanProperty(bean, property) {
        def path = property.tokenize(".[]")
        def value = bean
        for(sub in path) value = value != null ? value[sub.isNumber() ? sub as int : sub] : null
        if(value instanceof Date || value instanceof BigDecimal) value = value.format()
        return value
    }

    private attrsForPrefix(attrs, prefix) {
        def found = attrs.findAll { k, v -> k.startsWith("$prefix-") }
        found.each { k, v -> attrs.remove(k) }
        found.collectEntries { k, v -> [k.replace("$prefix-", ''), v] }
    }

    private uncamelize(str) { str?.split('\\.')?.last()?.replaceAll(/\B[A-Z]/) { ' ' + it.toLowerCase() }?.capitalize() }

    private addHtmlClass(attrs, String... htmlClass) { attrs.class = ((attrs.class?.split(' ') ?: []) + htmlClass.join(' ')).join(' ') }

    private mergeHtmlAttrs(Map... attrs) {
        def htmlAttrs = [:]
        attrs.each { map ->
            map.each { k, v ->
                if(htmlAttrs[k] && k.toLowerCase() in ['class', 'style']) htmlAttrs[k] += ' ' + v
                else htmlAttrs[k] = v
            }
        }
        return htmlAttrs
    }

}
