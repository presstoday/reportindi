package reportindi

import org.codehaus.groovy.grails.web.pages.discovery.GrailsConventionGroovyPageLocator as GCGPL

class UITagLib {

    static namespace = "ui"

    GCGPL groovyPageLocator

    def link = { attrs, body ->
        def controller = attrs.remove('controller') ?: controllerName
        def action = attrs.remove('action') ?: actionName
        def linkParams = attrs.remove('params') ?: [:]
        def disabled = attrs.remove('disabled') ?: false
        def href = attrs.href = disabled ? 'javascript:void(0)' : createLink(controller: controller, action: action, params: linkParams)
        if(disabled) {
            addHtmlClass(attrs, 'ui-state-disabled')
            attrs.remove('onclick')
        }
        def hrefAsText = attrs.remove('hrefAsText')
        def content = attrs.remove('text') ?: body()?.trim() ?: (hrefAsText ? href : '')
        def icon = attrs.remove('icon')
        def html = new HtmlBuilder(out)
        html.a(attrs) {
            if(icon) span(class: "ui-icon $icon") { mkp.yield '' }
            mkp.yieldUnescaped content
        }
    }

    def email = { attrs, body ->
        def email = attrs.remove('email') ?: throwTagError('Missing required attribute [email] for tag [email]')
        attrs.href = "mailto:$email"
        attrs.class = ((attrs.class?.split(' ') ?: []) + 'email-link').join(' ')
        def html = new HtmlBuilder(out)
        html.a(attrs) { mkp.yieldUnescaped body()?.trim() ?: email }
    }

    def renderTemplate = { attrs ->
        def template = attrs.template ?: throwTagError('Missing required attribute [template] for tag [renderTemplate]')
        def source = template.startsWith('/') ? groovyPageLocator.findTemplateByPath(template) : groovyPageLocator.findTemplate(template)
        if(source) out << render(template: template)
    }

    def button = { attrs, body ->
        def buttonOptions = ['disabled', 'icons', 'label', 'text', 'create']
        def text = ''
        if(attrs.text && attrs.text instanceof String) {
            text = attrs.remove('text') ?: ''
            attrs.text = true
        } else attrs.text = false
        def content = text instanceof String ? text : body()?.trim()
        def id = attrs.id = attrs.id ?: randomId('button-')
        if(attrs.icon && attrs.icon instanceof String) attrs.icons = [primary: attrs.remove('icon')]
        if(attrs.action || attrs.controller || attrs.params) {
            def action = attrs.remove('action') ?: actionName
            def controller = attrs.remove('controller') ?: controllerName
            attrs.href = createLink(controller: controller, action: action, params: attrs.remove('params'))
        }
        def options = jsOptions(attrs, buttonOptions)
        def html = new HtmlBuilder(out)
        if(attrs.containsKey('href')) html.a(attrs) { mkp.yieldUnescaped content }
        else html.button(attrs) { mkp.yieldUnescaped content }
        r.script() { /$("#$id").button($options);/ }
    }

    def list = { attrs ->
        def data = attrs.remove('data')?.grep() ?: []
        def tag = attrs.remove('tag') != 'ol' ? 'ul' : 'ol'
        def liAttrs = attrsForPrefix(attrs, 'li')
        def html = new HtmlBuilder(out)
        html."$tag"(attrs) { data.each { value -> html.li(liAttrs) { mkp.yieldUnescaped value } } }
    }

    def times = { attrs, body ->
        def n = (attrs.remove("n") ?: 0) as int
        n.times { out << body() }
    }

    private randomId(prefix = '') {
        if(request['random-id'] == null) request['random-id'] = []
        def id = prefix + String.random(10)
        while(id in request['random-id']) id = prefix + String.random(10)
        request['random-id'] << id
        return id
    }

    private jsOptions(attrs, options = []) {
        def found = attrs.findAll { k, v -> k in options }, parse
        found.each { k, v -> attrs.remove(k) }
        parse = { map ->
            return '{' + map.collect { k, v ->
                def value = v
                if(v instanceof String && v.startsWith('function')) value = /$v/
                else if(v instanceof String) value = /"$v"/
                else if(v instanceof Map) value = parse(v)
                /"$k": $value/
            }.join(', ') + '}'
        }
        return parse(found)
    }

    private attrsForPrefix(attrs, prefix) {
        def found = attrs.findAll { k, v -> k.startsWith("$prefix-") }
        found.each { k, v -> attrs.remove(k) }
        found.collectEntries { k, v -> [k.replace("$prefix-", ''), v] }
    }

    private addHtmlClass(attrs, String... htmlClass) { attrs.class = ((attrs.class?.split(' ') ?: []) + htmlClass.join(' ')).join(' ') }

}
