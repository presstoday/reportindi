package reportindi

class FlashTagLib {

    static namespace = "flash"

    def error = { attrs ->
        def error = flash.remove('error')
        if(error) {
            def html = new HtmlBuilder(out)
            html.div(class: "flash ui-corner-all error") { mkp.yieldUnescaped error }
        }
    }

    def message = { attrs ->
        def message = flash.remove('message')
        if(message) {
            def html = new HtmlBuilder(out)
            html.div(class: "flash ui-corner-all message") { mkp.yieldUnescaped message }
            r.script() { /setTimeout(function() { $(".flash.message").animate({opacity: 0, height: 0, margin: 0, padding: 0}, 500, function() { $(this).remove(); }); }, 5000);/ }
        }
    }

    def info = { attrs ->
        def info = flash.remove('info')
        if(info) {
            def html = new HtmlBuilder(out)
            html.div(class: "flash ui-corner-all info") { mkp.yieldUnescaped info }
            r.script() { /setTimeout(function() { $(".flash.info").animate({opacity: 0, height: 0, margin: 0, padding: 0}, 500, function() { $(this).remove(); }); }, 5000);/ }
        }
    }

    def alert = { attrs ->
        def alert = flash.remove("alert") ?: attrs.message
        if(alert) {
            def html = new HtmlBuilder(out)
            html.div(title: "Attenzione!", class: "alert") { mkp.yieldUnescaped alert }
            r.script() { /$(".alert").dialog({modal: true, width: 500});/ }
        }
    }

    def output = { attrs ->
        out << error(attrs)
        out << message(attrs)
        out << info(attrs)
        out << alert(attrs)
    }
}
