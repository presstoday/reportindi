package reportindi

import grails.util.Environment
import processate.Log
import processate.Sinistri


class CaricaSinistriJob {
    def IAssicurWebService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: 'caricaSinistri', cronExpression: '0 00 21 * * ?'
        }
    }
    def execute() {
        log.info 'CaricaSinistriJob triggered'
        caricaSinistri()
    }
    def caricaSinistri() {
        def logg
        def resultwebS = IAssicurWebService.getSRE()
        def sinistri = resultwebS.sinistri
        if(sinistri){
            sinistri.each{ sinistro->
                def sinistroDB=Sinistri.findByCodiceSx(sinistro.codice)
                def nSinistro=new Sinistri()
                nSinistro.codiceSx=sinistro.codice
                nSinistro.compagnia=sinistro.compagnia
                nSinistro.evento=sinistro.evento
                nSinistro.persona=sinistro.persona
                nSinistro.iter=sinistro.iter
                def dataIter = Date.parse('dd/MM/yyyy', sinistro.dataIter)
                nSinistro.dataIter=dataIter
                if (nSinistro.save(flush: true)) {
                    logg =new Log(parametri: "${sinistro.codice} salvato correttamente", operazione: "caricamento sinistri", pagina: "CaricaSinistriJob")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                } else {
                    logg =new Log(parametri: "Errore caricamento sinistro ${nSinistro.errors}", operazione: "caricamento sinistri", pagina: "CaricaSinistriJob")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }
        }

    }
}
