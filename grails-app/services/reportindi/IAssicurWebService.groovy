package reportindi

import processate.Log
import wslite.rest.RESTClientException

class IAssicurWebService {

    static transactional = false
    def iAssicurClient
    def grailsApplication

    def query(String sql) throws RESTClientException {
        def logg
        def path = grailsApplication.config.iAssicur.azioni.query.path
        def url = iAssicurClient.url
        iAssicurClient.url = grailsApplication.config.iAssicur.urlProduzione
        log.debug "iAssicur Webservice: ${url}${path} -> ${sql}"
        logg =new Log(parametri: "iAssicur Webservice: ${url}${path} -> ${sql}", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def response = null
        try {
            response = iAssicurClient.get(path: path, query: [SQL: sql])
        } finally {
            iAssicurClient.url = url
        }
        log.debug "Response status: ${response.statusCode} (${response.statusMessage})"
        logg =new Log(parametri: "Response status: ${response.statusCode} (${response.statusMessage})", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return response
    }
    def getSRE( ) {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def giornoOdierno=new Date().format("EEE")
        println dataOdierna
        def dataggprima

        select = "select COMPAGNIA,EVENTO,PERSONA,ITER,DATAITER FROM SRE WHERE ITER IN (181,198,163,178,179,180,197,164,172,173,174,165,189,190,170,192,217,166,175,176,177,259,260,167,169,168,237,238,219,187,196,194,236,218,199,230,195,239,182,183,184,188,185,186,231,232,233) and dataiter =${dataOdierna}"
        //select = "select COMPAGNIA,EVENTO,PERSONA,ITER,DATAITER FROM SRE WHERE ITER IN (181,198,163,178,179,180,197,164,172,173,174,165,189,190,170,192,217,166,175,176,177,259,260,167,169,168,237,238,219,187,196,194,236,218,199,230,195,239,182,183,184,188,185,186,231,232,233) and dataiter inrange 01.12.2016:30.01.2017"
        //select = "select COMPAGNIA,EVENTO,PERSONA,ITER,DATAITER FROM SRE WHERE ITER IN (181,198,163,178,179,180,197,164,172,173,174,165,189,190,170,192,217,166,175,176,177,259,260,167,169,168,237,238,219,187,196,194,236,218,199,230,195,239,182,183,184,188,185,186,231,232,233) and dataiter =12.01.2017"
        //println select
        logg =new Log(parametri: "query IAssicur ${select} ", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.codice = campo.text()
                                break
                            case 1: def compagnia = campo.text()
                                def index1 = compagnia.lastIndexOf("(")
                                def index2 = compagnia.lastIndexOf(")")
                                compagnia = compagnia[index1 + 1..<index2]
                                sinistro.compagnia = compagnia.toString().trim()
                                break
                            case 2: def evento = campo.text()
                                def index1 = evento.lastIndexOf("(")
                                def index2 = evento.lastIndexOf(")")
                                evento = evento[index1 + 1..<index2]
                                sinistro.evento = evento.toString().trim().toInteger()
                                break
                            case 3:  def persona = campo.text()
                                def index1 = persona.lastIndexOf("(")
                                def index2 = persona.lastIndexOf(")")
                                persona = persona[index1 + 1..<index2]
                                sinistro.persona = persona.toString().trim().toInteger()
                                break
                            case 4: def iter = campo.text()
                                def index1 = iter.lastIndexOf("(")
                                def index2 = iter.lastIndexOf(")")
                                iter = iter[index1 + 1..<index2]
                                sinistro.iter = iter.toString().trim().toInteger()
                                break
                            case 5: def dataIter  = campo.text()
                                sinistro.dataIter=dataIter.toString().trim()
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
}
