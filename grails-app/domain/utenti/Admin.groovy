package utenti

class Admin extends Utente {

    String telefono, email
    boolean isDeveloper

    static constraints = {
        telefono nullable: true, maxSize: 20
        email blank: false, email: true, maxSize: 40
    }

    static mapping = {
        table 'utenti_admin'
        isDeveloper defaultValue: '0'
    }
}
