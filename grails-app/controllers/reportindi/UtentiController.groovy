package reportindi

import utenti.Utente

class UtentiController {

    static defaultAction = "elenco"


    def login(String username, String password) {
        if(request.post) {
            def utente = Utente.findByUsernameAndPassword(username, password.encodeAsMD5())
            if(utente) {
                if(utente.bloccato) flash.error = "L'accesso per l'utente '$utente' è stato bloccato"
                else {
                    session.utente = utente
                    redirect controller: "cartelle", action: "elenco"
                }
            } else flash.error = 'Credenziali di accesso errate o inesistenti'
        }
        render view: 'login', layout: false
    }

    def logout() {
        session.invalidate()
        redirect action: 'login'
    }

}