package reportindi

import groovy.sql.Sql
import processate.Sinistri

class CartelleController {

    static defaultAction = "elenco"
    def cartelleService
    def dataSource_fonte
    def dataSource_frodi

    def elenco() {
        //CaricaSinistriJob.triggerNow()
        def db = fonteDb
        def dbf = frodiDb
        def risulta=[]
        def data1,data2
        def firstDayOfTheMonth
        def lastDayOfTheMonth
        if(params.containsKey("aggiorna")){
            def dataodierna = new Date()
            dataodierna = dataodierna.clearTime()
            def c = Calendar.getInstance()
            def cal = Calendar.getInstance()
            cal.setTime(dataodierna)
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH))
            firstDayOfTheMonth = cal.getTime()
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            lastDayOfTheMonth = cal.getTime()
            data1=""
            data2=""
        }else if(params.containsKey("filtraperdata")){
            if(params.data1 && params.data2){
                data1=params.data1
                data2=params.data2
                def c = Calendar.getInstance()
                def c2 = Calendar.getInstance()
                def date1=Date.parse("dd-MM-yyyy",params.data1)
                def date2=Date.parse("dd-MM-yyyy",params.data2)
                c.setTime(date1)
                c2.setTime(date2)
                firstDayOfTheMonth=c.getTime()
                lastDayOfTheMonth=c2.getTime()

            }else{
               return flash.error="inserire le date"
            }
        }else{
            def dataodierna = new Date()
            dataodierna = dataodierna.clearTime()
            def c = Calendar.getInstance()
            def cal = Calendar.getInstance()
            cal.setTime(dataodierna)
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH))
            firstDayOfTheMonth = cal.getTime()
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            lastDayOfTheMonth = cal.getTime()
        }
        def mappaMailGest = []
        def listMailGest=db.rows("select processate.idcartella, count(distinct date_format(processate.created,'%Y%m%d%h'),processate.idcartella,processate.email) as totaleM from stampaEmail.processate where processate.idcartella is not null and  date(processate.created) between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}' group by processate.idcartella")
        listMailGest.each() { cart ->
            mappaMailGest[cart.idcartella] = cart.totaleM
        }
        def mappaSinInd = []
        def listSinInd=db.rows("select riconosciute.idcartella, count(distinct date_format(riconosciute.created,'%Y%m%d%h'),riconosciute.idcartella,riconosciute.email_from) as totaleS from stampaEmail.riconosciute where riconosciute.idcartella is not null and date(riconosciute.created)  between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}' group by riconosciute.idcartella")
        listSinInd.each() { cart ->
            mappaSinInd[cart.idcartella] = cart.totaleS?:0
        }
        def mappaSinGest=[]
        def listSinGest=Sinistri.createCriteria().list(){
            not {'in'("iter",[186,233])}
            between('dataIter', firstDayOfTheMonth, lastDayOfTheMonth)
            projections {
                groupProperty 'persona'
                count()
            }
        }
        //listSinGest=dbf.rows("select sinistri_log.persona, count(*) as totaleC from frodi.sinistri_log  where sinistri_log.persona is not null and  date(sinistri_log.created)  between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}' group by  sinistri_log.persona")
        listSinGest.each(){cart->
            def personaid=cart[0]
            mappaSinGest[personaid]=cart[1]?:0
        }

        def mappaAperTel=[]
        def listApeTel=Sinistri.createCriteria().list(){
            'in'("iter",[186,233])
            between('dataIter', firstDayOfTheMonth, lastDayOfTheMonth)
            projections {
                groupProperty 'persona'
                count()
            }
        }
        listApeTel.each(){cart->
            def personaid=cart[0]
            mappaAperTel[personaid]=cart[1]?:0
        }
        def listCartelle=db.rows("select idcartelle, persona, nome from stampaEmail.cartelle where email != 'compagnie' and email !='mach1.pagamenti@gmail.com' and nome != 'Libera1' and nome != 'processate' and  nome !='APERTURE' and idcartelle is not null and persona is not null and stato != 0;")
            listCartelle.each() { cart ->
                def cartella=[nome:null,idcartella:null,mailGestit:0,sinGestit:0,sinIndi:0,apertTel:0]
                cartella.nome=cart.nome
                cartella.idcartella=cart.idcartelle
                //def mailGest= db.rows("select count(distinct date_format(processate.created,'%Y%m%d%h'),processate.idcartella,processate.email) as totaleM from stampaEmail.processate where processate.idcartella=${cart.idcartelle}  and date(processate.created) between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}' group by processate.idcartella;")
                cartella.mailGestit=mappaMailGest[cartella.idcartella]?:0

                //def sinGesti=db.rows("select  count(distinct date_format(riconosciute.created,'%Y%m%d%h'),riconosciute.idcartella,riconosciute.email_from) as totaleS from stampaEmail.riconosciute where idcartella=${cart.idcartelle}     between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}';")
                cartella.sinInd=mappaSinInd[cartella.idcartella]?:0

                //def cambiSt=dbf.rows("select count(*) as totaleC from frodi.sinistri_log  where sinistri_log.persona = ${cart.persona} and date(sinistri_log.created)  between '${firstDayOfTheMonth.format("yyyy-MM-dd")}' and '${lastDayOfTheMonth.format("yyyy-MM-dd")}';")
                cartella.sinGestit=mappaSinGest[cartella.idcartella]?:0
                cartella.apertTel=mappaAperTel[cartella.idcartella]?:0

                risulta<<cartella
            }

        render view: 'elenco', model: [cartelle: risulta.flatten(), data1:data1,data2:data2]
    }
    private def getFonteDb() {
        //println dataSource_rinnovi
        return new Sql(dataSource_fonte)
    }
    private def getFrodiDb() {
        //println dataSource_rinnovi
        return new Sql(dataSource_frodi)
    }
    def grails() { render view: '/grails' }



}
