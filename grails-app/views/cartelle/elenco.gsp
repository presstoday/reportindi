<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title></title>
        <r:external file="/less/applicazioni.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:script>


         $(".date1").change(function() {
             var dat1 = this.value;
             $("#data1").val( dat1);
             //params.data1=dat1;
         });
         $(".date2").change(function() {
             var dat2 = this.value;
             $("#data2").val( dat2);
             //params.data2=dat2;

         });
         $.fn.dataTable.ext.search.push(
                 function( settings, data, dataIndex ) {
                     var min = parseInt( $('#min').val(), 10 );
                     var max = parseInt( $('#max').val(), 10 );
                     var age = parseFloat( data[3] ) || 0; // use data for the age column

                     if ( ( isNaN( min ) && isNaN( max ) ) ||
                             ( isNaN( min ) && age <= max ) ||
                             ( min <= age   && isNaN( max ) ) ||
                             ( min <= age   && age <= max ) )
                     {
                         return true;
                     }
                     return false;
                 }
         );



         $(document).ready(function() {
             var table = $('#example').DataTable({
                 pageLength: 30,
                 oLanguage: {
                     sSearch: "Ricerca:",
                     oPaginate: {
                         sPrevious: "<<",
                         sNext: ">>"
                     },
                     sProcessing: "Processando dati",
                     sEmptyTable: "Dati non disponibili"
                 },
                 info:     false,
                 dom: 'Bfrtip',
                 buttons: [{
                     extend: 'excel',
                     text: 'Estrarre dati su excel'
                 }

                 ],
                 columnDefs: [
                     {className: "dt-center", "targets": "_all"}
                 ]
             });


         } );
        </r:script>
    </head>
    <body>
        <flash:output/>
            <form id="form-rango"  method="post" >
                <div class="range col-md-6 col-md-push-4">
                        <f:field type="date" class="date1" name="data1" id="date1" maxDate="0" value="${data1}" label="Da"/>
                        <f:field type="date" class="date2" name="data2" maxDate="0" id="date2" value="${data2}" label="a"/>
                        <f:button class="filtra-button" type="submit" name="filtraperdata" value="Filtra per data" id="filtraperdata"/>
                </div>
                <div>
                    <f:button class="aggiorna-button" type="submit" name="aggiorna" value="Aggiorna" id="aggiorna"/>
                </div>
                <table id="example" class="display" cellspacing="0" width="80%">
                    <thead>
                    <tr>
                        <th>Cartella</th>
                        <th>Mail gestite</th>
                        <th>Sinistri indicizzati</th>
                        <th>Sinistri gestiti</th>
                        <th>Aperture telefoniche</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="cartella" in="${cartelle}">
                        <tr>
                            <td>${cartella.nome}</td>
                            <td>${cartella.mailGestit}</td>
                            <td>${cartella.sinInd}</td>
                            <td>${cartella.sinGestit}</td>
                            <td>${cartella.apertTel}</td>
                        </tr>
                    </g:each>
                    </tbody>
                <tfoot>
                </tfoot>
                </table>
            </form>
    </body>
</html>