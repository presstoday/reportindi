<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Report - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <r:external file="/less/layout.less"/>
    <r:external file="/css/jquery-ui/overcast/jquery-ui.css"/>
    %{--<r:external file="/css/bootstrap.css"/>--}%
    <r:external file="/less/main.less"/>
    <r:external file="/less/login.less"/>
    <r:require module="jquery-ui"/>
    <r:layoutResources/>
    </head>
    <body>


        <div id="wrapper">
            <div id="main">
                <div id="body">
                    <div style="margin-top: 15px; margin-bottom: 15px;"> <h6>Report sinistri generati per utenti</h6>
                    </div>
                    <div class="col-md-8 col-md-push-5">
                        <r:img dir="images/mach1" file="logo_mach1.png" id="logo-mach1"/>
                    </div>
                    <form method="post" autocomplete="off">
                        <div class="col-md-12 col-md-pull-7">
                            <flash:error/>
                        </div>
                        <div class="div1 col-md-10">

                            <div class=" div2 col-md-10">
                                <f:field type="text" name="username" autofocus="true"/>
                            </div>
                            <div class=" div3 col-md-10">
                                <f:field type="password" name="password"/>
                            </div>
                            <div class="div4 col-md-10">
                                <f:submit value="Login" id="login"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="footer">
            <strong>MACH 1 SRL</strong>
            Direzione e Sede Legale Via Vittor Pisani, 13/B | 20124 Milano - TEL. 02 00638 057  FAX 02 62087266 -<br>
            CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603
        </div>
        <r:layoutResources/>
    </body>
</html>