<%@ page import="utenti.*" %>
<g:set var="utente" value="${session.utente}"/>
<h6 >Report Sinistri generati per utenti</h6>
<ul class="menu">
    <li id="logout"><g:link controller="utenti" action="logout">Logout</g:link></li>
</ul>
<r:script>
    $(".menu").buttonset();
    $(".ui-button", $("#logout").prev()).addClass("ui-corner-right");
    $(".ui-button", "#logout").addClass("ui-corner-left");
</r:script>