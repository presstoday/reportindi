<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Reporte - <g:layoutTitle default="${actionName}"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
	<r:external file="/less/layout.less"/>
	<r:external file="/less/main.less"/>
	<r:require module="jquery-ui"/>
	<g:layoutHead/>
	<r:layoutResources />
	<r:script>
		$("[title]").tooltip();
		$("body").mousemove(function(event) { $("#spinner").css({top: event.pageY, left: event.pageX + 16}); });
		$(document).ajaxStart(function() { $("#spinner").show(); }).ajaxStop(function() { $("#spinner").hide(); });
	</r:script>
</head>
<body>
<div id="wrapper">
	<div id="main">
		<div id="header" class="ui-widget-header"><ui:renderTemplate template="/menu"/></div>
		<div id="body"><g:layoutBody/></div>
	</div>
</div>
<div id="footer">
	<strong>MACH 1 SRL</strong>
	Direzione e Sede Legale Via Vittor Pisani, 13/B | 20124 Milano  - TEL. 02 00638 057  FAX 02 62087266 - <br>
	CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603
</div>
<g:img dir="images" file="spinner.gif" id="spinner"/>
<r:layoutResources />
</body>
</html>
