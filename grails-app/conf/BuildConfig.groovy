grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "target/${appName}##${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        mavenRepo "http://repo.grails.org/grails/plugins/"

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {

        runtime 'mysql:mysql-connector-java:5.1.27'
        runtime "com.github.groovy-wslite:groovy-wslite:1.1.3"

        //compile "net.sf.uadetector:uadetector-resources:2013.11"
        compile "com.itextpdf:itextpdf:5.4.5"
        compile "org.apache.poi:poi:3.9"
        compile "org.apache.poi:poi-ooxml:3.9"
        compile "org.apache.commons:commons-lang3:3.1"
        compile "commons-net:commons-net:3.3"
        compile "org.apache.camel:camel-ftp:2.13.0"
        compile "com.github.groovy-wslite:groovy-wslite:1.1.3"
        /*compile "joda-time:joda-time:2.9.5"
        compile "org.grails.plugins:excel-import:1.0.0" //novembre 2016
        compile "org.apache.poi:ooxml-schemas:1.1"*/
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.

        // runtime 'mysql:mysql-connector-java:5.1.22'
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":resources:1.2.1"
        runtime ":cache-headers:1.1.5"
        runtime ":zipped-resources:1.0.1"
        runtime ":cached-resources:1.1"
        build ":tomcat:$grailsVersion"
        compile ':cache:1.1.1'
        compile ":lesscss-resources:1.3.3"
        compile ":quartz:1.0.1"
        compile ":mail:1.0.1"
        compile ":hibernate-filter:0.3.2"
        compile ":routing:1.3.2"
    }
}
