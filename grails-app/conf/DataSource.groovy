dataSource {
    //logSql = true
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "root"
    password = "poi"
    url = "jdbc:mysql://ironman/reportcartelle"
    formatSql = true
    properties {
        maxActive = -1
        minEvictableIdleTimeMillis = 1800000
        timeBetweenEvictionRunsMillis = 1800000
        numTestsPerEvictionRun = 3
        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = true
        validationQuery = "SELECT 1"
    }
}
dataSource_fonte {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "root"
    password = "poi"
    url = "jdbc:mysql://ironman/stampaemail"
    formatSql = true
    properties {
        maxActive = -1
        minEvictableIdleTimeMillis = 1800000
        timeBetweenEvictionRunsMillis = 1800000
        numTestsPerEvictionRun = 3
        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = true
        validationQuery = "SELECT 1"
    }
}
dataSource_frodi {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "root"
    password = "poi"
    url = "jdbc:mysql://ironman/frodi"
    formatSql = true
    properties {
        maxActive = -1
        minEvictableIdleTimeMillis = 1800000
        timeBetweenEvictionRunsMillis = 1800000
        numTestsPerEvictionRun = 3
        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = true
        validationQuery = "SELECT 1"
    }
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {

    test {
        dataSource {
            url = "jdbc:mysql://ironman/reportcartelle"
            password= "Pre55T0day!"
        }
        dataSource_fonte {
            url = "jdbc:mysql://ironman/stampaEmail"
            password= "Pre55T0day!"
        }
        dataSource_frodi {
            url = "jdbc:mysql://ironman/frodi"
            password= "Pre55T0day!"
        }
    }
    production {
        dataSource {
            url = "jdbc:mysql://ironman/reportcartelle"
            password= "Pre55T0day!"
        }
        dataSource_fonte {
            url = "jdbc:mysql://ironman2/stampaEmail"
            password= "poi"
        }
        dataSource_frodi {
            url = "jdbc:mysql://ironman2/frodi"
            password= "poi"
        }
    }
}
