class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{ constraints { } }
		"/"(controller: "cartelle", action: "elenco")
		"/login"(controller: 'utenti', action: 'login')
		"/logout"(controller: 'utenti', action: 'logout')
		"/grails"(controller: 'cartelle', action: 'grails')
		"500"(view:'/error')
	}
}
