modules = {
    jquery {
        defaultBundle 'core'
        //resource url: '/js/jquery-1.10.2.min.js', disposition: 'head'
        resource url: '/js/jquery-1.12.4.js', disposition: 'head'
        resource url: '/js/jquery.browser.js', disposition: 'head'
        resource url: '/js/numeral/numeral.js', disposition: 'head'
        resource url: '/js/numeral/it.js', disposition: 'head'
        resource url: '/js/jquery.utils.js', disposition: 'head'
        resource url: '/js/utils.js', disposition: 'head'
        resource url: '/js/jquery.js', disposition: 'head'
        resource url: '/js/jquery.dataTables.js', disposition: 'head'
        resource url: '/js/dataTables.buttons.min.js', disposition: 'head'
        resource url: '/js/buttons.flash.min.js', disposition: 'head'
        resource url: '/js/jszip.min.js', disposition: 'head'
        resource url: '/js/dataTables.bootstrap.min.js', disposition: 'head'

    }

    'jquery-ui' {
        dependsOn 'jquery'
        defaultBundle 'core'
        resource url: '/css/jquery-ui/cupertino/jquery-ui.css', disposition: 'head'
        /*resource url: '/css/select2/select2.css', disposition: 'head'*/
        resource url: '/js/jquery-ui-1.10.3.min.js', disposition: 'head'
        resource url: '/js/jquery.ui.combobox.js', disposition: 'head'
        resource url: '/js/jquery.ui.datepicker-it.js', disposition: 'head'
        resource url: '/js/jquery-ui.config.js', disposition: 'head'
        /*resource url: '/js/select2/select2.js', disposition: 'head'
        resource url: '/js/select2/select2_locale_it.js', disposition: 'head'*/
    }
}