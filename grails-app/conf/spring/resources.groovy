// Place your Spring DSL code here
import wslite.http.auth.HTTPBasicAuthorization
import wslite.rest.RESTClient
import wslite.http.HTTPClient
beans = {
    localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
        defaultLocale = new Locale("it","IT")
        java.util.Locale.setDefault(defaultLocale)
    }
    customPropertyEditorRegistrar(editor.CustomEditorRegistrar)
    iAssicurHTTPClient(HTTPClient) {
        sslTrustAllCerts = true
    }

    iAssicurAuthorization(HTTPBasicAuthorization) {
        username = application.config.iAssicur.username
        password = application.config.iAssicur.password
    }
    iAssicurClient(RESTClient){
        httpClient = ref("iAssicurHTTPClient")
        url = application.config.iAssicur.url
        defaultContentTypeHeader = "text/xml"
        defaultAcceptHeader = "text/xml"
        authorization = ref("iAssicurAuthorization")
    }
}
