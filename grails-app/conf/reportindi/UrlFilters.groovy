package reportindi

class UrlFilters {

    def filters = {
        loginCheck(action: 'login|logout', invert: true) {
            before = {
                if(session.utente == null) {
                    redirect(controller: "utenti", action: "login")
                    return false
                }
            }
        }

        adminCheck(controller: 'utenti') {
            before = {
                if(actionName in ['login', 'logout'] || session.utente instanceof utenti.Admin) return true
                else {
                    redirect(controller: 'cartelle', action: 'elenco')
                    return false
                }
            }
        }
    }
}
