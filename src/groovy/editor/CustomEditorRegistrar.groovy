package editor

import org.springframework.beans.PropertyEditorRegistrar as PERegistrar
import org.springframework.beans.PropertyEditorRegistry as PERegistry
import org.springframework.beans.propertyeditors.CustomDateEditor

import java.text.SimpleDateFormat

class CustomEditorRegistrar implements PERegistrar {

    void registerCustomEditors(PERegistry registry) {
        registry.registerCustomEditor(BigDecimal, new BigDecimalEditor())
        registry.registerCustomEditor(Date, new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true))
    }

}
