package usertype

import org.apache.commons.lang3.ObjectUtils
import org.hibernate.HibernateException
import org.hibernate.engine.SessionImplementor
import org.hibernate.type.Type
import org.hibernate.usertype.CompositeUserType

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException;

abstract class ImmutableType implements CompositeUserType {
	
    abstract String[] getPropertyNames();
    
    abstract Type[] getPropertyTypes();
    
    abstract Object getPropertyValue(final Object component, final int property) throws HibernateException;
    
    abstract void setPropertyValue(final Object component, final int property, final Object setValue) throws HibernateException;
    
    abstract Object nullSafeGet(final ResultSet resultSet, final String[] names, final SessionImplementor paramSessionImplementor, final Object paramObject) throws HibernateException, SQLException;
    
    abstract void nullSafeSet(final PreparedStatement preparedStatement, final Object value, final int property, final SessionImplementor sessionImplementor) throws HibernateException, SQLException;
    
    Serializable disassemble(final Object value, final SessionImplementor paramSessionImplementor) throws HibernateException { return (Serializable)value; }
    
    Object assemble(final Serializable cached, final SessionImplementor sessionImplementor, final Object owner) throws HibernateException { return cached; }
    
    Object replace(final Object original, final Object target, final SessionImplementor paramSessionImplementor, final Object owner) throws HibernateException { return original; }
    
    abstract Class returnedClass();
    
    boolean equals(final Object x, final Object y) throws HibernateException { return ObjectUtils.equals(x, y); }
    
    int hashCode(final Object value) throws HibernateException { return value.hashCode(); }
    
    boolean isMutable() { return false; }
    
    Object deepCopy(final Object value) throws HibernateException { return value; }
    
}

