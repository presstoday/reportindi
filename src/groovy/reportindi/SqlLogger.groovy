package reportindi

import org.apache.log4j.Level
import org.apache.log4j.Logger

class SqlLogger {
	
    static def log(Closure fn) {
        def sqlLogger = Logger.getLogger("org.hibernate.SQL")
        def typeLogger = Logger.getLogger("org.hibernate.type")
        sqlLogger.setLevel(Level.DEBUG)
        typeLogger.setLevel(Level.TRACE)
        def result = fn()
        sqlLogger.setLevel(Level.OFF)
        typeLogger.setLevel(Level.OFF)
        return result
    }
}

