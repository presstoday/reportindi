package reportindi

class Grid {
	
    boolean headers = true
    boolean paginate = true
    boolean colgroup = true
    List columns = []
    String emptyMessage = 'Non sono presenti elementi'
    String caption = ''
    
    void setColumns(List cols) { columns = cols.grep { column -> column.size() > 0 } }
    
    boolean isValid() {
        def valid = true
        valid &= columns.size() > 0
        return valid
    }
    
    String toString() {
        return """
            headers: $headers
            paginate: $paginate
            colgroup: $colgroup
            columns: $columns
            emptyMessage: $emptyMessage
            caption: $caption
        """
    }
}

