package reportindi

import groovy.xml.MarkupBuilder

class HtmlBuilder extends MarkupBuilder {
	
    HtmlBuilder(writer) {
        super(writer)
        omitNullAttributes = true
    }
    
}

